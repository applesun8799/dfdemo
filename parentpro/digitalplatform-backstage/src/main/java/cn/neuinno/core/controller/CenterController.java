package cn.neuinno.core.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
* @author jaysun
* @version 创建时间:2016年11月22日
*/

@Controller
public class CenterController {
	
	/**
	 * ModelAndView:跳转视图+数据 
	 * void :异步时ajax
	 * String :跳转视图+Model
	 */
	@RequestMapping(value="/test/index.do")
	public String  index(Model model){
		
		return "index";
	}
}
