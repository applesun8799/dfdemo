package cn.neuinno.core.dao;

import cn.neuinno.core.bean.TestTb;

/**
* @author jaysun
* @version 创建时间:2016年11月21日
*/
public interface TestTbDao {
	public void insertTestTb(TestTb testTb);
}
