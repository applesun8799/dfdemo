package cn.neuinno.core.bean;

import java.util.Date;

/**
 * @author jaysun
 * @version 创建时间:2016年11月21日
 */
public class TestTb {
	
	private Integer id;
	private String name;
	private Date birthday;

	@Override
	public String toString() {
		return "TestTb [id=" + id + ", name=" + name + ", birthday=" + birthday + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
}
