package cn.neuinno.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.neuinno.core.bean.TestTb;
import cn.neuinno.core.dao.TestTbDao;

/**
* @author jaysun
* @version 创建时间:2016年11月22日
*/

@Service
@Transactional
public class TestTbServiceImpl implements TestTbService {
	
	@Autowired
	private TestTbDao testTbDao;

	@Override
	public void insertTestTb(TestTb testTb) {
		testTbDao.insertTestTb(testTb);
		//throw new RuntimeException();
	}

}
