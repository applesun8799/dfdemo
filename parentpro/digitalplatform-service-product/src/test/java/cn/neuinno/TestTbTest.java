package cn.neuinno;
/**
* @author jaysun
* @version 创建时间:2016年11月21日
*/

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.neuinno.core.bean.TestTb;
import cn.neuinno.core.service.TestTbService;

@RunWith(SpringJUnit4ClassRunner.class) 
@ContextConfiguration(locations={"classpath:application-context.xml"}) 
public class TestTbTest {

    @Autowired
	//private TestTbDao testTbDao;
    private TestTbService testTbService;
	
	@Test
	public void testAdd() throws Exception{
		
		TestTb testTb = new TestTb();
		testTb.setName("八大锤事务");
		testTb.setBirthday(new Date());
		
		//testTbDao.insertTestTb(testTb);
		testTbService.insertTestTb(testTb);
	}

	
}
