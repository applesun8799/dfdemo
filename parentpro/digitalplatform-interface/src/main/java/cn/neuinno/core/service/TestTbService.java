package cn.neuinno.core.service;

import cn.neuinno.core.bean.TestTb;

/**
* @author jaysun
* @version 创建时间:2016年11月22日
*/
public interface TestTbService {

	public void insertTestTb(TestTb testTb);
}
